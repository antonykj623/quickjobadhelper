package com.centroid.quickjobadhelper.domains;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ads implements Serializable {

    @SerializedName("ads_id")
    @Expose
     String adsId;
    @SerializedName("ads_type")
    @Expose
     String adsType;
    @SerializedName("ads_url")
    @Expose
     String adsUrl;


    @SerializedName("ad_link")
    @Expose
     String ad_link;
    @SerializedName("ad_description")
    @Expose
     String ad_description;

    @SerializedName("isend")
    @Expose
    boolean isend;




    public Ads() {
    }

    public boolean isIsend() {
        return isend;
    }

    public void setIsend(boolean isend) {
        this.isend = isend;
    }

    public String getAd_link() {
        return ad_link;
    }

    public void setAd_link(String ad_link) {
        this.ad_link = ad_link;
    }

    public String getAd_description() {
        return ad_description;
    }

    public void setAd_description(String ad_description) {
        this.ad_description = ad_description;
    }

    public String getAdsId() {
        return adsId;
    }

    public void setAdsId(String adsId) {
        this.adsId = adsId;
    }

    public String getAdsType() {
        return adsType;
    }

    public void setAdsType(String adsType) {
        this.adsType = adsType;
    }

    public String getAdsUrl() {
        return adsUrl;
    }

    public void setAdsUrl(String adsUrl) {
        this.adsUrl = adsUrl;
    }
}
