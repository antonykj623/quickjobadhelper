package com.centroid.quickjobadhelper.webservicehelper;

import com.centroid.quickjobadhelper.domains.Ads;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface WebServiceimpl {

    @POST("checkLogin")
    @FormUrlEncoded
    Call<JsonObject> loginRequest(@Field("username") String username, @Field("password") String password);


    @POST("getAllAds")
    @FormUrlEncoded
    Call<List<Ads>> getAllAds(@Header("Token") String token,@Field("page")int page);

    @POST("removeAd")
    @FormUrlEncoded
    Call<JsonObject> deleteAd(@Header("Token") String token, @Field("ad_id") String ads_id);



    @Multipart
    @POST("addNewAdvertisement")
    Call<JsonObject> addNewAdvertisement(@Part MultipartBody.Part file, @Part("ad_type")RequestBody ad_type,
                                     @Header("Token")String token, @Part("ad_link")RequestBody ad_link, @Part("ad_description")RequestBody ad_description

    );


    @Multipart
    @POST("EditAd")
    Call<JsonObject> EditAdWithfile(@Part MultipartBody.Part file, @Part("ad_type")RequestBody ad_type,
                                         @Header("Token")String token, @Part("ad_link")RequestBody ad_link, @Part("ad_description")RequestBody ad_description,@Part("ad_id")RequestBody ad_id

    );


    @Multipart
    @POST("EditAd")
    Call<JsonObject> EditAd( @Part("ad_type")RequestBody ad_type,
                            @Header("Token")String token, @Part("ad_link")RequestBody ad_link,
                             @Part("ad_description")RequestBody ad_description,@Part("ad_id")RequestBody ad_id

    );
}
