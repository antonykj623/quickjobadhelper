package com.centroid.quickjobadhelper.constants;

import android.content.Context;

import android.text.TextUtils;
import android.util.Log;


import androidx.annotation.NonNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * @author Sooraj Soman on 11/1/2018
 */
public class ApiConstants {


    public static final String BASEURL = "http://www.zoomoutdoormedia.in/Nearajob/api/adApi/";
    public static final String SHAREDPREF = "MYPREF";
    public static final String USERTOKEN = "TOKEN";

    public static final String USERBRANCH = "USERBRANCH";

    public static final String SEARCHTOKEN = "SEARCHTOKEN";

    public static final String LASTADDDRESS = "LASTADDDRESS";


    public static final String imgbaseurl="http://www.zoomoutdoormedia.in/Nearajob/api/ads/";






    public static final String LOGINSTATE = "";
    public static final String BEARER = "Bearer ";
    public static final String FCMUSERTOKEN = "FCMUSERTOKEN";

    public static final String firebase_Url="https://fcm.googleapis.com/";

    public static final String FirebaseAuthKey="AAAAjQV1_c4:APA91bHuJJ7amY9K0mmsTsL08Sz_ycW7VyIayX3_ZPj07FKITF-vN0RIyrML8x4mf7eUiqbRqpedQKL1UikWK5tfVJhRmceW2ZySWqIgsoHVZA2SR8wYpbyGWfghc1S2pQRLehleGIrh";


public static Set<Integer> temp_wishlist=new HashSet<>();



    public static Boolean isNonEmpty(@NonNull String data) {
        return !TextUtils.isEmpty(data) && !data.equals("");
    }

    public static String amountFormatter(@NonNull String data) {

        return "\u20B9 " + data;
    }

    public static double isNonEmpty(@NonNull String data, String cgst, String sgst) {
        Double total = null;
        try {
            Double data1 = Double.valueOf(data);
            Double data2 = Double.valueOf(cgst);
            Double data3 = Double.valueOf(sgst);
            total = data1 + (data1 / (data2 * 100)) + (data1 / (data3 * 100));
            return total;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return total;
    }


    public static String getSecretKey(Context context, String username, String password)
    {

        String uuid= UUID.randomUUID().toString();

//        String username=new PreferenceHelper(context).getData(Literals.user);
//        String password=new PreferenceHelper(context).getData(Literals.password);

        Log.e("user",getMD5(username));
        Log.e("pass",getMD5(password));


        String keycombo=getMD5(username)+uuid+getMD5(password)+"&"+uuid;
        Log.e("Keycombo",keycombo);

        return keycombo;
    }


    public static String getMD5(String keycombo)
    {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(keycombo.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static final String table_user="User";


}
