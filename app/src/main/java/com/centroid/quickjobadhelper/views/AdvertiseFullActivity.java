package com.centroid.quickjobadhelper.views;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.centroid.quickjobadhelper.R;
import com.centroid.quickjobadhelper.adapters.AdvertiseAdapter;
import com.centroid.quickjobadhelper.constants.ApiConstants;
import com.centroid.quickjobadhelper.domains.Ads;
import com.centroid.quickjobadhelper.preferencehelper.PreferenceHelper;
import com.centroid.quickjobadhelper.progress.ProgressFragment;
import com.centroid.quickjobadhelper.webservicehelper.RestService;
import com.centroid.quickjobadhelper.webservicehelper.WebServiceimpl;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdvertiseFullActivity extends AppCompatActivity {

    Ads ads;

    ImageView imgad,imgplaybton,imgback,imgDelete,imgEdit;

    TextView txtview,txtAdinfo,txtAdtype,txtAdlink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertise_full);
        getSupportActionBar().hide();

        ads=(Ads) getIntent().getSerializableExtra("advertise");


        imgad=this.findViewById(R.id.imgad);
        imgback=findViewById(R.id.imgback);
        imgDelete=findViewById(R.id.imgDelete);
        imgplaybton=this.findViewById(R.id.imgplaybton);

        txtview=this.findViewById(R.id.txtview);
        txtAdinfo=this.findViewById(R.id.txtAdinfo);

        txtAdtype=this.findViewById(R.id.txtAdtype);
        txtAdlink=this.findViewById(R.id.txtAdlink);
        imgEdit=findViewById(R.id.imgEdit);


        DisplayMetrics displayMetrics = new DisplayMetrics();
       getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int w=width;
        double h=w/1.7;

 imgad.setLayoutParams(new FrameLayout.LayoutParams(w,(int)h));



        Glide.with(this).load(ApiConstants.imgbaseurl + ads.getAdsUrl()).into(imgad);


        if(ads.getAd_link().equals(""))
        {
            txtview.setVisibility(View.GONE);
        }
        else{

            txtview.setVisibility(View.VISIBLE);
        }


        if(ads.getAdsType().equals("0"))
        {
            imgplaybton.setVisibility(View.GONE);
            txtAdtype.setText("Image");
        }
        else{

            imgplaybton.setVisibility(View.VISIBLE);
            txtAdtype.setText("Video");
        }

        txtAdinfo.setText(ads.getAd_description());


        if(ads.getAd_link().equals(""))
        {
            txtAdlink.setText("No link available");
        }
        else {

            txtAdlink.setText(ads.getAd_link());
        }








        txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i= new Intent(Intent.ACTION_VIEW, Uri.parse(ads.getAd_link()));
                startActivity(i);
            }
        });

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                AlertDialog.Builder builder=new AlertDialog.Builder(AdvertiseFullActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to Delete this Ad now ?");
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                       removeAd();


                    }
                });

                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });

                builder.show();










            }
        });



        imgplaybton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(ads.getAdsType().equals("1"))
                {
                    //imgplaybton.visibility=View.GONE


                    Intent intent=new Intent(AdvertiseFullActivity.this, VideoFullScreenActivity.class);

                    intent.putExtra("file",ads.getAdsUrl());
                  startActivity(intent);


                }
            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(AdvertiseFullActivity.this, EditAdActivity.class);

                intent.putExtra("ads",ads);
                startActivity(intent);

            }
        });

    }

    public void removeAd() {


        final ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "j");


        WebServiceimpl client = RestService.getClient();

        Call<JsonObject> jsonObjectCall=client.deleteAd(new PreferenceHelper(AdvertiseFullActivity.this).getData(ApiConstants.USERTOKEN),ads.getAdsId());

        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressFragment.dismiss();


                if(response.body()!=null)
                {

                    try{

                        JSONObject jsonObject=new JSONObject(response.body().toString());

                        if(jsonObject.getInt("status")==1)
                        {


                            Toast.makeText(AdvertiseFullActivity.this,"Deleted Successfully",Toast.LENGTH_SHORT).show();



                            onBackPressed();




                        }
                        else {

                            Toast.makeText(AdvertiseFullActivity.this," failed",Toast.LENGTH_SHORT).show();


                        }




                    }catch (Exception e)
                    {

                    }

                }
                else {

                    Toast.makeText(AdvertiseFullActivity.this,"failed",Toast.LENGTH_SHORT).show();


                }




            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                t.printStackTrace();
                progressFragment.dismiss();
            }
        });

    }
    }
