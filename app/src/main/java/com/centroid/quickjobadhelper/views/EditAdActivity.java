package com.centroid.quickjobadhelper.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.centroid.quickjobadhelper.R;
import com.centroid.quickjobadhelper.constants.ApiConstants;
import com.centroid.quickjobadhelper.domains.Ads;
import com.centroid.quickjobadhelper.preferencehelper.PreferenceHelper;
import com.centroid.quickjobadhelper.progress.ProgressFragment;
import com.centroid.quickjobadhelper.webservicehelper.RestService;
import com.centroid.quickjobadhelper.webservicehelper.WebServiceimpl;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAdActivity extends AppCompatActivity {

    Ads ads;

    ImageView imgback,imgAd,imgTick;

    Button btnupload;
    Spinner spAdType;

    TextView txtFilename;

    int typ=-1;

    File filetoupload=null;

    EditText input_description,input_link;

    LinearLayout layout_adimg,layout_advideo;

    VideoView videoAd;

    int filepicked=0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ad);
        getSupportActionBar().hide();

        ads=(Ads) getIntent().getSerializableExtra("ads");



        imgback=findViewById(R.id.imgback);
        btnupload=findViewById(R.id.btnupload);
        imgAd=findViewById(R.id.imgAd);
        videoAd=findViewById(R.id.videoAd);
        spAdType=findViewById(R.id.spAdType);
        txtFilename=findViewById(R.id.txtFilename);
        input_description=findViewById(R.id.input_description);

        input_link=findViewById(R.id.input_link);
        imgTick=findViewById(R.id.imgTick);

        //btnsubmit=findViewById(R.id.btnsubmit);

        layout_adimg=findViewById(R.id.layout_adimg);
        layout_advideo=findViewById(R.id.layout_advideo);



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        setAdData();



        btnupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(filetoupload!=null)
                {

                    filetoupload=null;
                    txtFilename.setText("No files selected");
                    filepicked=0;
                    btnupload.setText("Upload");

                    layout_adimg.setVisibility(View.GONE);
                    layout_advideo.setVisibility(View.GONE);


                }
                else {


                    String type = (String) spAdType.getSelectedItem();

                    if (type.equals("Image")) {

                        typ = 0;


                        pickImage();


                    } else if (type.equals("Video")) {

                        typ = 1;

                        pickvideo();
                    } else {


                        Toast.makeText(EditAdActivity.this, "Please choose Advertisement file type", Toast.LENGTH_SHORT).show();
                    }


                }



            }
        });


        imgTick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(typ!=-1)
                {

                    if(!input_description.getText().toString().equals(""))
                    {





                            EditAdvertisement();



//                        }
//
//                        else {
//
//                            Toast.makeText(EditAdActivity.this, "Please upload file", Toast.LENGTH_SHORT).show();
//
//                        }


                    }
                    else {

                        Toast.makeText(EditAdActivity.this, "Please enter Advertisement description", Toast.LENGTH_SHORT).show();

                    }

                }
                else {

                    Toast.makeText(EditAdActivity.this, "Please choose Advertisement type", Toast.LENGTH_SHORT).show();

                }


            }
        });



    }

    private void setAdData()
    {

        if(ads.getAdsType().equals("1"))
        {

            typ=1;

            spAdType.setSelection(2);
            layout_adimg.setVisibility(View.GONE);
            layout_advideo.setVisibility(View.VISIBLE);

            Uri uri = Uri.parse(ApiConstants.imgbaseurl+ads.getAdsUrl());
            videoAd.setVideoURI(uri);

            videoAd.setMediaController(new MediaController(this));
            videoAd.start();


        }
        else {

            spAdType.setSelection(1);

            typ=0;
            layout_adimg.setVisibility(View.VISIBLE);
            layout_advideo.setVisibility(View.GONE);


            DisplayMetrics displayMetrics = new DisplayMetrics();
          getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;

            int w=width;
            double h=w/1.7;

        imgAd.setLayoutParams(new LinearLayout.LayoutParams(w,(int)h));

            Glide.with(EditAdActivity.this).load(ApiConstants.imgbaseurl + ads.getAdsUrl()).into(imgAd);





        }

        txtFilename.setText(ads.getAdsUrl());

        btnupload.setText("Remove");



        input_description.setText(ads.getAd_description());
        input_link.setText(ads.getAd_link());

        filetoupload=new File(ApiConstants.imgbaseurl+ads.getAdsUrl());

    }







    public void EditAdvertisement()
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"j");

        WebServiceimpl client = RestService.getClient();


        RequestBody requestbodyinput_description= RequestBody.create(MediaType.parse("text/plain"), input_description.getText().toString());

        RequestBody requestbodyinput_link= RequestBody.create(MediaType.parse("text/plain"), input_link.getText().toString());

        RequestBody requestbodyinput_ad_type= RequestBody.create(MediaType.parse("text/plain"), String.valueOf(typ));

        RequestBody requestbodyinput_ad_id= RequestBody.create(MediaType.parse("text/plain"), String.valueOf(ads.getAdsId()));


        if(filepicked==1) {


            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), filetoupload);

            MultipartBody.Part body = MultipartBody.Part.createFormData("file", filetoupload.getName(), requestFile);


            Call<JsonObject> jsonObjectCall = client.EditAdWithfile(body, requestbodyinput_ad_type, new PreferenceHelper(EditAdActivity.this).getData(ApiConstants.USERTOKEN), requestbodyinput_link, requestbodyinput_description,requestbodyinput_ad_id);

            jsonObjectCall.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    progressFragment.dismiss();


                    if (response.body() != null) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.body().toString());

                            if (jsonObject.getInt("status") == 1) {


                                Toast.makeText(EditAdActivity.this, "upload success", Toast.LENGTH_SHORT).show();


                                onBackPressed();


                            } else {

                                Toast.makeText(EditAdActivity.this, " failed", Toast.LENGTH_SHORT).show();


                            }


                        } catch (Exception e) {

                        }


                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressFragment.dismiss();
                    t.printStackTrace();

                }
            });

        }
        else {


            Call<JsonObject> jsonObjectCall = client.EditAd( requestbodyinput_ad_type, new PreferenceHelper(EditAdActivity.this).getData(ApiConstants.USERTOKEN), requestbodyinput_link, requestbodyinput_description,requestbodyinput_ad_id);

            jsonObjectCall.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    progressFragment.dismiss();


                    if (response.body() != null) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.body().toString());

                            if (jsonObject.getInt("status") == 1) {


                                Toast.makeText(EditAdActivity.this, "upload success", Toast.LENGTH_SHORT).show();


                                onBackPressed();


                            } else {

                                Toast.makeText(EditAdActivity.this, " failed", Toast.LENGTH_SHORT).show();


                            }


                        } catch (Exception e) {

                        }


                    }

                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    progressFragment.dismiss();
                    t.printStackTrace();

                }
            });




        }



    }















    public void pickvideo()
    {
        if(ContextCompat.checkSelfPermission(EditAdActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(intent , 33);
        }
        else {


            ActivityCompat.requestPermissions(EditAdActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},33);
        }
    }



    public void pickImage()
    {

        if(ContextCompat.checkSelfPermission(EditAdActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {

            Intent i = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, 2);
        }
        else {


            ActivityCompat.requestPermissions(EditAdActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},2);
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==2) {

            pickImage();
        }
        else if(requestCode==33){

            pickvideo();

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 2 && resultCode == RESULT_OK
                && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);


            filetoupload=new File(picturePath);

            filepicked=1;

            txtFilename.setText(filetoupload.getName());
            btnupload.setText("Remove");


            cursor.close();



        }

        if (requestCode == 33 && resultCode == RESULT_OK
                && null != data) {

            Uri selectedImage = data.getData();

            String videopath=getPath(selectedImage);
            filetoupload=new File(videopath);
            filepicked=1;
            txtFilename.setText(filetoupload.getName());
            btnupload.setText("Remove");

        }




    }


    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
}
