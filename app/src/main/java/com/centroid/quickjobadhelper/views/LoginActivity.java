package com.centroid.quickjobadhelper.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.centroid.quickjobadhelper.R;
import com.centroid.quickjobadhelper.constants.ApiConstants;
import com.centroid.quickjobadhelper.preferencehelper.PreferenceHelper;
import com.centroid.quickjobadhelper.progress.ProgressFragment;
import com.centroid.quickjobadhelper.webservicehelper.RestService;
import com.centroid.quickjobadhelper.webservicehelper.WebServiceimpl;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button btnlogin;

    EditText input_email,input_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        btnlogin=findViewById(R.id.btnlogin);
        input_email=findViewById(R.id.input_email);

        input_password=findViewById(R.id.input_password);


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!input_email.getText().toString().equals(""))
                {

                    if(!input_password.getText().toString().equals(""))
                    {


                        checkUser();

                    }
                    else {

                        Toast.makeText(LoginActivity.this,"Enter the password",Toast.LENGTH_SHORT).show();
                    }


                }
                else {

                    Toast.makeText(LoginActivity.this,"Enter the user name",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }



    public void checkUser()
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"j");

        WebServiceimpl client = RestService.getClient();


        Call<JsonObject>jsonObjectCall=client.loginRequest(input_email.getText().toString(),input_password.getText().toString());

        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                progressFragment.dismiss();

                if(response.body()!=null)
                {

                  try{

                      JSONObject jsonObject=new JSONObject(response.body().toString());

                      if(jsonObject.getInt("status")==1)
                      {


                          Toast.makeText(LoginActivity.this,"Login success",Toast.LENGTH_SHORT).show();


                          String token=jsonObject.getString("token");

                          new PreferenceHelper(LoginActivity.this).putData(ApiConstants.USERTOKEN,token);


                          startActivity(new Intent(LoginActivity.this,HomeActivity.class));

                          finish();




                      }
                      else {

                          Toast.makeText(LoginActivity.this,"Login failed",Toast.LENGTH_SHORT).show();


                      }




                  }catch (Exception e)
                  {

                  }



                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressFragment.dismiss();

                t.printStackTrace();

            }
        });

    }


}
