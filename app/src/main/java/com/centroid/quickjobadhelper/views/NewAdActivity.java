package com.centroid.quickjobadhelper.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.centroid.quickjobadhelper.R;
import com.centroid.quickjobadhelper.constants.ApiConstants;
import com.centroid.quickjobadhelper.preferencehelper.PreferenceHelper;
import com.centroid.quickjobadhelper.progress.ProgressFragment;
import com.centroid.quickjobadhelper.webservicehelper.RestService;
import com.centroid.quickjobadhelper.webservicehelper.WebServiceimpl;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewAdActivity extends AppCompatActivity {


    ImageView imgback;

    Button btnupload,btnsubmit;
    Spinner spAdType;

    TextView txtFilename;

    int typ=-1;

    File filetoupload=null;

    EditText input_description,input_link;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ad);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        btnupload=findViewById(R.id.btnupload);
        spAdType=findViewById(R.id.spAdType);
        txtFilename=findViewById(R.id.txtFilename);
        input_description=findViewById(R.id.input_description);

        input_link=findViewById(R.id.input_link);

        btnsubmit=findViewById(R.id.btnsubmit);



        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(typ!=-1)
                {

                   if(!input_description.getText().toString().equals(""))
                   {



                       if(filetoupload!=null) {

                           uploadAdvertisement();
                       }

                       else {

                           Toast.makeText(NewAdActivity.this, "Please upload file", Toast.LENGTH_SHORT).show();

                       }


                   }
                   else {

                       Toast.makeText(NewAdActivity.this, "Please enter Advertisement description", Toast.LENGTH_SHORT).show();

                   }

                }
                else {

                    Toast.makeText(NewAdActivity.this, "Please choose Advertisement type", Toast.LENGTH_SHORT).show();

                }


            }
        });




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        btnupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(filetoupload!=null)
                {

                    filetoupload=null;
                    txtFilename.setText("No files selected");
                    btnupload.setText("Upload");


                }
                 else {


                    String type = (String) spAdType.getSelectedItem();

                    if (type.equals("Image")) {

                        typ = 0;


                        pickImage();


                    } else if (type.equals("Video")) {

                        typ = 1;

                        pickvideo();
                    } else {


                        Toast.makeText(NewAdActivity.this, "Please choose Advertisement file type", Toast.LENGTH_SHORT).show();
                    }


                }


            }
        });


    }




    public void uploadAdvertisement()
    {

        final ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"j");

        WebServiceimpl client = RestService.getClient();


        RequestBody requestbodyinput_description= RequestBody.create(MediaType.parse("text/plain"), input_description.getText().toString());

        RequestBody requestbodyinput_link= RequestBody.create(MediaType.parse("text/plain"), input_link.getText().toString());

        RequestBody requestbodyinput_ad_type= RequestBody.create(MediaType.parse("text/plain"), String.valueOf(typ));




            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), filetoupload);

            MultipartBody.Part body = MultipartBody.Part.createFormData("file", filetoupload.getName(), requestFile);



        Call<JsonObject> jsonObjectCall=client.addNewAdvertisement(body,requestbodyinput_ad_type,new PreferenceHelper(NewAdActivity.this).getData(ApiConstants.USERTOKEN),requestbodyinput_link,requestbodyinput_description);

        jsonObjectCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                progressFragment.dismiss();


                if(response.body()!=null)
                {

                    try{

                        JSONObject jsonObject=new JSONObject(response.body().toString());

                        if(jsonObject.getInt("status")==1)
                        {


                            Toast.makeText(NewAdActivity.this,"upload success",Toast.LENGTH_SHORT).show();


                            onBackPressed();




                        }
                        else {

                            Toast.makeText(NewAdActivity.this," failed",Toast.LENGTH_SHORT).show();


                        }




                    }catch (Exception e)
                    {

                    }



                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressFragment.dismiss();
                t.printStackTrace();

            }
        });



    }








    public void pickvideo()
    {
        if(ContextCompat.checkSelfPermission(NewAdActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {

            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(intent , 33);
        }
        else {


            ActivityCompat.requestPermissions(NewAdActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},33);
        }
    }



    public void pickImage()
    {

        if(ContextCompat.checkSelfPermission(NewAdActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED) {

            Intent i = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, 2);
        }
        else {


            ActivityCompat.requestPermissions(NewAdActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},2);
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode==2) {

            pickImage();
        }
        else if(requestCode==33){

            pickvideo();

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 2 && resultCode == RESULT_OK
                && null != data) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);


            filetoupload=new File(picturePath);

            txtFilename.setText(filetoupload.getName());
            btnupload.setText("Remove");


            cursor.close();



        }

        if (requestCode == 33 && resultCode == RESULT_OK
                && null != data) {

            Uri selectedImage = data.getData();

            String videopath=getPath(selectedImage);
            filetoupload=new File(videopath);

            txtFilename.setText(filetoupload.getName());
            btnupload.setText("Remove");

        }




    }


    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
}
