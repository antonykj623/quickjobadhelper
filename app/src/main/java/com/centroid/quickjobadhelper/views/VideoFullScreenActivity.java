package com.centroid.quickjobadhelper.views;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.centroid.quickjobadhelper.R;
import com.centroid.quickjobadhelper.constants.ApiConstants;

public class VideoFullScreenActivity extends AppCompatActivity {

    VideoView videoViewRelative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_full_screen);
        getSupportActionBar().hide();
        videoViewRelative=findViewById(R.id.videoViewRelative);

       String videostring=getIntent().getStringExtra("file");



        Uri uri = Uri.parse(ApiConstants.imgbaseurl+videostring);
        videoViewRelative.setVideoURI(uri);

        videoViewRelative.setMediaController(new MediaController(this));
        videoViewRelative.start();


    }
}
