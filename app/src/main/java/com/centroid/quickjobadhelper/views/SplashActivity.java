package com.centroid.quickjobadhelper.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.centroid.quickjobadhelper.R;
import com.centroid.quickjobadhelper.constants.ApiConstants;
import com.centroid.quickjobadhelper.preferencehelper.PreferenceHelper;

public class SplashActivity extends AppCompatActivity {

    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(new PreferenceHelper(SplashActivity.this).getData(ApiConstants.USERTOKEN).equals(""))
                {

                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();

                }
                else {

                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    finish();

                }

            }
        },3000);
    }
}
