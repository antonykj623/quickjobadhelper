package com.centroid.quickjobadhelper.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.centroid.quickjobadhelper.R;
import com.centroid.quickjobadhelper.adapters.AdvertiseAdapter;
import com.centroid.quickjobadhelper.constants.ApiConstants;
import com.centroid.quickjobadhelper.domains.Ads;
import com.centroid.quickjobadhelper.preferencehelper.PreferenceHelper;
import com.centroid.quickjobadhelper.progress.ProgressFragment;
import com.centroid.quickjobadhelper.webservicehelper.RestService;
import com.centroid.quickjobadhelper.webservicehelper.WebServiceimpl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    RecyclerView recyclerad;

    ImageView imglogout;
    FloatingActionButton fab;

    boolean isend=false,isloading=false;

    int page=1;

    List<Ads> ads=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        recyclerad=findViewById(R.id.recyclerad);
        imglogout=findViewById(R.id.imglogout);
        fab=findViewById(R.id.fab);

        recyclerad.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int offset = recyclerView.computeVerticalScrollOffset();
                int extent = recyclerView.computeVerticalScrollExtent();
                int range = recyclerView.computeVerticalScrollRange();

                float percentage = (100.0f * offset / (float)(range - extent));

                int a=(int)percentage;





                    if (!isend) {

                        if (a==100&&!isloading) {

                            page = ads.size() + 1;
                            getAllAds();

                        }
                    }

            }
        });




        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(HomeActivity.this,NewAdActivity.class));


            }
        });


        imglogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder=new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to logout now ?");
                builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                        new PreferenceHelper(HomeActivity.this).putData(ApiConstants.USERTOKEN,"");

                        startActivity(new Intent(HomeActivity.this,LoginActivity.class));

                        finish();


                    }
                });

                builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });

                builder.show();


            }
        });

        getAllAds();



    }

    @Override
    protected void onRestart() {
        super.onRestart();

        page=1;
        ads.clear();
        isend=false;
        isloading=false;
        getAllAds();
    }

    public void getAllAds()
    {


//        final ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"j");

        isloading=true;


        WebServiceimpl client = RestService.getClient();

        Call<List<Ads>> jsonObjectCall=client.getAllAds(new PreferenceHelper(HomeActivity.this).getData(ApiConstants.USERTOKEN),page);


        jsonObjectCall.enqueue(new Callback<List<Ads>>() {
            @Override
            public void onResponse(Call<List<Ads>> call, Response<List<Ads>> response) {

               // progressFragment.dismiss();

                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {
                        ads.addAll(response.body());


                        AdvertiseAdapter advertiseAdapter=new AdvertiseAdapter(HomeActivity.this,ads);
                        recyclerad.setLayoutManager(new GridLayoutManager(HomeActivity.this,2));

                        recyclerad.setAdapter(advertiseAdapter);

                        recyclerad.scrollToPosition(page-1);

                        isloading=false;

                        isend=response.body().get(0).isIsend();

                    }
                    else {



                    }


                }
                else {

                    Toast.makeText(HomeActivity.this,"failed",Toast.LENGTH_SHORT).show();


                }

            }

            @Override
            public void onFailure(Call<List<Ads>> call, Throwable t) {

               // progressFragment.dismiss();
                t.printStackTrace();

            }
        });
    }
}
