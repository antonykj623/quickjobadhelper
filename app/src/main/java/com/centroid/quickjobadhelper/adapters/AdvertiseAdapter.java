package com.centroid.quickjobadhelper.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.centroid.quickjobadhelper.R;
import com.centroid.quickjobadhelper.constants.ApiConstants;
import com.centroid.quickjobadhelper.domains.Ads;
import com.centroid.quickjobadhelper.views.AdvertiseFullActivity;
import com.centroid.quickjobadhelper.views.VideoFullScreenActivity;

import java.util.List;

public class AdvertiseAdapter  extends RecyclerView.Adapter<AdvertiseAdapter.AdvertiseHolder> {

    Context context;
    List<Ads>li;

    public AdvertiseAdapter(Context context, List<Ads> ads) {
        this.context = context;
        this.li = ads;
    }

    public class AdvertiseHolder extends RecyclerView.ViewHolder{
        
        ImageView imgad,imgplaybton;
        
        TextView txtview,txtAdinfo;

        public AdvertiseHolder(@NonNull View itemView) {
            super(itemView);
            imgad=itemView.findViewById(R.id.imgad);
            imgplaybton=itemView.findViewById(R.id.imgplaybton);

            txtview=itemView.findViewById(R.id.txtview);
            txtAdinfo=itemView.findViewById(R.id.txtAdinfo);
        }
    }

    @NonNull
    @Override
    public AdvertiseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adholder,parent,false);


        return new AdvertiseHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AdvertiseHolder holder,final int position) {


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((AppCompatActivity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        int w=width/2;
        double h=w/1.7;

        holder.imgad.setLayoutParams(new FrameLayout.LayoutParams(w,(int)h));



        Glide.with(context).load(ApiConstants.imgbaseurl + li.get(position).getAdsUrl()).into(holder.imgad);


        if(li.get(position).getAd_link().equals(""))
        {
            holder.txtview.setVisibility(View.GONE);
        }
        else{

            holder.txtview.setVisibility(View.VISIBLE);
        }


        if(li.get(position).getAdsType().equals("0"))
        {
            holder.imgplaybton.setVisibility(View.GONE);
        }
        else{

            holder.imgplaybton.setVisibility(View.VISIBLE);
        }

        holder.txtAdinfo.setText(li.get(position).getAd_description());
        

        


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    //holder.imgplaybton.visibility=View.GONE


                    Intent intent=new Intent(context, AdvertiseFullActivity.class);

                    intent.putExtra("advertise",li.get(position));
                    context.startActivity(intent);



            }
        });
        

       


        
        

    }

    @Override
    public int getItemCount() {
        return li.size();
    }
}
